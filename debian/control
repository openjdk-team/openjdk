Source: openjdk-25
Section: java
Priority: optional
Maintainer: OpenJDK Team <openjdk-25@packages.debian.org>
Uploaders: Matthias Klose <doko@ubuntu.com>
Build-Depends: debhelper (>= 11),
  m4, lsb-release, zip, unzip,
  sharutils, gawk, cpio, procps, wdiff, pkgconf, fastjar (>= 2:0.96-0ubuntu2),
  time, strip-nondeterminism, debugedit (>= 4.16),
  jtreg7 (>= 7.4+1~) <!nocheck>, libtestng7-java <!nocheck>, xvfb <!nocheck>, xauth <!nocheck>, xfonts-base <!nocheck>, libgl1-mesa-dri [!x32] <!nocheck>, xfwm4 <!nocheck>, x11-xkb-utils <!nocheck>, dbus-x11 <!nocheck>, libasmtools-java <!nocheck>, xvfb  <!nocheck>,
  autoconf, automake, ant, ant-optional,
  g++-14 <!cross>,
  openjdk-24-jdk-headless:native | openjdk-25-jdk-headless:native,
  libxtst-dev, libxi-dev, libxt-dev, libxaw7-dev, libxrender-dev, libcups2-dev, libasound2-dev, liblcms2-dev, libxinerama-dev, libkrb5-dev, xsltproc, libpcsclite-dev, libxrandr-dev, libelf-dev, libfontconfig-dev, libfreetype-dev, libharfbuzz-dev,
  libffi-dev, libffi-dev:native,
  zlib1g-dev:native, zlib1g-dev, libattr1-dev, libpng-dev, libjpeg-dev, libgif-dev, systemtap-sdt-dev [!powerpc !ppc64 !ppc64el !sh4 !s390x],
  libnss3-dev (>= 2:3.17.1),
  openjdk-25-jdk-headless <cross>,
Build-Depends-Indep: graphviz, pandoc,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://openjdk.java.net/
Vcs-Browser: https://salsa.debian.org/openjdk-team/openjdk
Vcs-Git: https://salsa.debian.org/openjdk-team/openjdk.git

Package: openjdk-25-jdk-headless
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jre-headless (= ${binary:Version}),
  ${shlibs:Depends}, ${misc:Depends}
Suggests: openjdk-25-demo, openjdk-25-source
Provides: java-sdk-headless (= ${vm:Version}), java-compiler
Description: OpenJDK Development Kit (JDK) (headless)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.

Package: openjdk-25-jre-headless
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: ${jredefault:Depends}, ${cacert:Depends},
  ${jcommon:Depends}, ${dlopenhl:Depends},
  ${mountpoint:Depends},
  ${shlibs:Depends}, ${misc:Depends}
Recommends: ${dlopenhl:Recommends},
  ${fontmanager:Recommends}, ${jsound:Recommends}
Suggests: libnss-mdns,
  fonts-dejavu-extra,
  fonts-ipafont-gothic, fonts-ipafont-mincho, fonts-wqy-microhei | fonts-wqy-zenhei, fonts-indic,
Provides: java-runtime-headless (= ${vm:Version}),
  ${defaultvm:Provides}, ${jvm:Provides}
Description: OpenJDK Java runtime, using ${vm:Name} (headless)
 Minimal Java runtime - needed for executing non GUI Java programs,
 using ${vm:Name}.

Package: openjdk-25-jdk
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jre (= ${binary:Version}),
  openjdk-25-jdk-headless (= ${binary:Version}),
  ${shlibs:Depends}, ${misc:Depends}
Recommends: libxt-dev
Suggests: openjdk-25-demo, openjdk-25-source, visualvm
Provides: java-sdk (= ${vm:Version}), java-compiler
Description: OpenJDK Development Kit (JDK)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.

Package: openjdk-25-jre
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jre-headless (= ${binary:Version}),
  ${dlopenjre:Depends}, ${fontmanager:Depends}, ${jsound:Depends},
  ${shlibs:Depends}, ${misc:Depends}
Recommends: ${dlopenjre:Recommends}, ${bridge:Recommends}, fonts-dejavu-extra
Provides: java-runtime (= ${vm:Version})
Description: OpenJDK Java runtime, using ${vm:Name}
 Full Java runtime environment - needed for executing Java GUI and Webstart
 programs, using ${vm:Name}.

Package: openjdk-25-demo
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jre (= ${binary:Version}),
  ${shlibs:Depends}, ${misc:Depends}
Description: Java runtime based on OpenJDK (demos and examples)
 OpenJDK Java runtime

Package: openjdk-25-source
Architecture: all
Multi-Arch: foreign
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jdk (>= ${source:Version}),
  ${misc:Depends}
Description: OpenJDK Development Kit (JDK) source files
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains the Java programming language source files
 (src.zip) for all classes that make up the Java core API.

Package: openjdk-25-doc
Section: doc
Pre-Depends: ${dpkg:Depends}
Architecture: all
Multi-Arch: foreign
Priority: optional
Depends: ${misc:Depends},
  libjs-jquery,
  libjs-jquery-ui,
  libjs-jquery-ui-theme-base,
Suggests: openjdk-25-jdk
Description: OpenJDK Development Kit (JDK) documentation
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains the API documentation.

Package: openjdk-25-dbg
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Priority: optional
Section: debug
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jre-headless (= ${binary:Version}),
   ${misc:Depends},${shlibs:Depends}
Recommends: openjdk-25-jre (= ${binary:Version})
Suggests: openjdk-25-jdk (= ${binary:Version})
Conflicts: ${dbg:Conflicts}
Description: Java runtime based on OpenJDK (debugging symbols)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains the debugging symbols.

Package: openjdk-25-testsupport
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 loong64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Depends: openjdk-25-jdk (= ${binary:Version}),
  build-essential, xfwm4, xvfb, dbus-x11, libatk-wrapper-java,
  libatk-wrapper-java-jni, jtreg7 (>= 7.4+1~), libtestng7-java,
  ${shlibs:Depends},
  ${misc:Depends}
Description: Java runtime based on OpenJDK (regression test support)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains all the binary files needed to run the
 OpenJDK autopkg tests.

Package: openjdk-25-jvmci-jdk
Architecture: amd64
Multi-Arch: same
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: ${jredefault:Depends}, ${cacert:Depends},
  ${jcommon:Depends}, ${dlopenhl:Depends},
  ${mountpoint:Depends},
  ${shlibs:Depends}, ${misc:Depends}
Description: JVMCI-enabled SDK for building graalvm
  This package provides a JVMCI-enabled GraalVM builder Java SDK.

Package: openjdk-25-jre-zero
Architecture: amd64 arm64 ppc64 ppc64el riscv64 s390x
Multi-Arch: same
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-25-jre-headless (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Provides: ${zerovm:Provides}
Description: Alternative JVM for OpenJDK, using Zero
 The package provides an alternative runtime using the Zero VM. Built on
 architectures in addition to the Hotspot VM as a debugging aid for those
 architectures which don't have a Hotspot VM.
 .
 The VM is started with the option `-zero'. See the README.Debian for details.
